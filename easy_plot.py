import html
import svg
import math
import random # for use in demo

# TODO: [X] Create Scatter chart
# TODO: [X] Create Line chart
# TODO: [X] Add axes to Line chart
# TODO: [ ] Create Bar chart
# TODO: [X] Add axes to scatter
# TODO: [X] refactor axes 
# TODO: [X] add text numbers to  axes 
# TODO: [X] add axes labels

# we assume this element exists
panel = doc["panel"]  # select an elemtnt with id="panel"
availx = float(panel.parent.width)
availy = float(panel.parent.height)

MARGIN = 50  # buffer width in pixels
DEFAULT_R = 8  # default radius for circles in scatter chart
MINOR_X_PITCH = 40 # default minor tick mark pitch in pixels
MINOR_Y_PITCH = 40 # default minor tick mark pitch in pixels


def clear_panel():
    while len(panel.childNodes) > 0:
        for child in panel: # iteration on child nodes
            panel.remove(child)

# below is some random stuff associated with pie_chart()
colors = ["C8E0A2","A6BED1","E4CC85","D7D7D7","90AF97","698EA8",
        "BFA166","A8ADB0","FF6600"]

legend = None

def add_title(title_str):
    title = svg.text('',x=availx/2, y=25,
        font_size=22,text_anchor="middle",
        style={"stroke":"black"})
    panel <= title
    title.text = title_str 

default_pie_data = {"chocolate":100, "glaze":10, "custard":30, "sprinkles":15, "peanut":25}

# pie_chart 
def pie_chart(in_data):
    ray = 100
    if not in_data:
      in_data = default_pie_data
      print("using the default data: ",default_pie_data)

    global legend

    clear_panel()

    add_title("Pie Chart")
    # zone for legend
    legend = svg.text('',x=350,y=150,
        font_size=20,text_anchor="middle",
        style={"stroke":"black"})
    panel <= legend

    paths = {}
    style={"fill-opacity": 1,"stroke":"white","stroke-width": 1}
    width = 3.8*ray
    height = 2.2*ray
    x_center = 150
    y_center = 160
    x = x_center
    y = y_center-ray
    total = sum(in_data.values())
    cumul = 0
    for i,key in enumerate(in_data):
        angle1 = 2*math.pi*cumul
        cumul += float(in_data[key])/total
        angle = 2*math.pi*cumul
        x_end = x_center + ray*math.cos((math.pi/2)-angle)
        y_end = y_center - ray*math.sin((math.pi/2)-angle)
        path = "M%s,%s " %(x_center,y_center)
        path += "L%s,%s " %(int(x),int(y))
        if angle-angle1 <= math.pi:
            path += "A%s,%s 0 0,1 " %(ray,ray)
        else:
            path += "A%s,%s 0 1,1 " %(ray,ray)
        path += "%s,%s z" %(int(x_end),int(y_end))
        x,y = x_end,y_end
        color = colors[i % len(colors)]
        style["fill"]='#'+color
        path = svg.path(d=path,style=style,
            onMouseOver="show_legend('%s')" %key,
            onMouseOut="hide_legend()")
        panel <= path
        paths[key]=path

    print("sucessfully created pie chart")
    
def show_legend(key):
    legend.text = key

def hide_legend():
    legend.text = ''

class AutoScale():
    def __init__(self, xarr, yarr):
        self.minx = min(xarr)
        print("minx: %f" % self.minx)
        self.maxx = max(xarr)
        self.miny = min(yarr)
        self.maxy = max(yarr)

        self.xscale = (availx - 2*MARGIN) / float(self.maxx - self.minx)
        self.yscale = (availy - 2*MARGIN)/ float(self.maxy - self.miny)

    # pixel to original coordantes on x axis
    def p2ox(self, pixel):
        return pixel/self.xscale + self.minx 

    def p2oy(self, pixel):
        return pixel/self.yscale + self.miny 

def add_axes(targ_svg, au):
    # add axes
    # y-axis 
    disty = availy - 2 * MARGIN
    targ_svg <= svg.line(x1=0, y1=0, x2=0, y2=disty, stroke="black",
                           stroke_opacity=1.0)
    # x-axis
    distx = availx - 2 * MARGIN
    targ_svg <= svg.line(x1=0, y1=0, x2=distx, y2=0,
                           stroke="black", stroke_opacity=1.0)
    # minor x axes
    for py in range(0, disty, MINOR_X_PITCH):
        targ_svg <= svg.line(x1=0, y1=py, x2=distx, y2=py,
                               stroke="black", stroke_opacity=0.25)
        # add marker text
        targ_svg <= svg.text("%.01f" % au.p2oy(py), text_anchor = "middle", 
                         transform="scale(1,-1)", x=-MARGIN/4, y=-py)
    # minor y axes
    for px in range(0, distx, MINOR_Y_PITCH):
        #xloc = MINOR_Y_PITCH*i
        targ_svg <= svg.line(y1=0, x1=px, y2=disty, x2=px,
                               stroke="black", stroke_opacity=0.25)
        # add marker text
        targ_svg <= svg.text("%.01f" % au.p2ox(px), text_anchor = "middle", 
                         transform="scale(1,-1)", y=MARGIN/4, x=px)
    

# this is sperated from add_axes() so you can add labels without lines
def add_ax_labels(targ_svg):
    # Y Axis Label
    targ_svg <= svg.text("Y Axis Label", text_anchor = "middle", 
                         transform="rotate(-90)", x=-availy/2, y=MARGIN/2)
    # X Axis Label
    targ_svg <= svg.text("X Axis Label", text_anchor = "middle", 
                         x=availx/2, y=(availy - MARGIN/2) )

d_xarr = [-2, 1, 1.1, 3, 5, 5.4, 10]; d_yarr = [-2, 1, 5, 3, 5, 3.3, 4.6]
#     Line Chart code
#  arguments: line_chart(xarray, yarray, <options>) 
def line_chart(xarr, yarr):
    if not xarr and not yarr:
        print("creating a line chart with the default data: ",d_xarr,d_yarr)
        xarr = d_xarr; yarr = d_yarr

    clear_panel()
    add_title("Line Chart")

    a = AutoScale(xarr, yarr)

    # set up margin and axes transformations 
    margin_wrap = svg.g(id="marginwrap", transform="translate(50,50)")
    cord_wrap1 = svg.g(id="coord_rapper1", transform="scale(1,-1)")
    cord_wrap0 = svg.g(id="coord_rapper0", transform="translate(0,%d)" % (availy - 2*MARGIN) )

    # add axes and labels
    add_axes(cord_wrap1, a)
    add_ax_labels(panel)

    # add line segments
    stroke_wrap = svg.g(id="stroke_rapper", stroke="#2ca02c", 
                        fill="#2ca02c", fill_opacity=0.5, stroke_opacity=1)
 
    for i in range(len(xarr) - 1):
        stroke_wrap <= svg.path(d="M%f %f L%f %f" % 
                                (a.xscale*(xarr[i] - a.minx), 
                                 a.yscale*(yarr[i] - a.miny), 
                                 a.xscale*(xarr[i+1] - a.minx), 
                                 a.yscale*(yarr[i+1] - a.miny) ) ) 

    cord_wrap1 <= stroke_wrap
    # applly transformations
    cord_wrap0 <= cord_wrap1
    margin_wrap <= cord_wrap0
    panel <= margin_wrap 
    print("sucessfully created line chart")


#     Scatter Chart code
#  arguments: line_chart(xarray, yarray, <options>) 
def scatter_chart(xarr, yarr, rad=DEFAULT_R):
    if not xarr and not yarr:
        xarr=[]; yarr=[]
        for i in range(-10,30):
          xarr.append(i+2*random.random())
          yarr.append(i+2*random.random()) 
        print("creating a scatter plot with random data")

    clear_panel()
    add_title("Scatter Chart")

    a = AutoScale(xarr, yarr)

    # set up SVG transformations
    margin_wrap = svg.g(id="marginwrap", transform="translate(50,50)")
    cord_wrap1 = svg.g(id="coord_rapper1", transform="scale(1,-1)")
    cord_wrap0 = svg.g(id="coord_rapper0", transform="translate(0,%d)" % (availy - 2*MARGIN) )

    # add axes and labels
    add_axes(cord_wrap1, a)
    add_ax_labels(panel)

    # add data points
    for i, x in enumerate(xarr):
        color = "#%s" % colors[i % len(colors)]
        cord_wrap1 <= svg.circle(cx=a.xscale*(x - a.minx), 
                            cy=a.yscale*(yarr[i] - a.miny), 
                            r=DEFAULT_R, fill=color,
                            stroke=color, stroke_width=1)
    
    cord_wrap0 <= cord_wrap1
    margin_wrap <= cord_wrap0
    panel <= margin_wrap 
    print("sucessfully created scatter chart")
