This is: easy_plot, an SVG plotting library for [Brython](http://brython.info/).  
Easy_plot allows you to quickly generate SVG charts.  

Demo: [pbharrin.neocities.org](http://pbharrin.neocities.org/)

# To install:

## option 1. 

copy the file easy_plot.py to your Brython src/Lib/ directory.  

## option 2. 

make easy_plot.py available in the same directory as the Brython code.  

# To use:
`import easy_plot as ep`

`ep.scatter_chart(xarr, yarr)`

By default it assumes there is an svg element called panel in the page.  

Calling any of the plots without any arguments `ep.scatter_chart()` will generate a demonstration of that chart.  

